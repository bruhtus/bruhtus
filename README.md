> i go normal from time to time 👌, "social media?" [¯\_(ツ)_/¯](https://solo.to/bruhtus)

<img width="400" src="https://github-readme-stats.vercel.app/api?username=bruhtus&show_icons=true&hide_border=true">

<a href="https://github.com/bruhtus">
<img src="https://img.shields.io/badge/platform-%20linux-blue"
alt="Platform: linux" />
<a/>

<a href="https://github.com/bruhtus">
<img src="https://visitor-badge.glitch.me/badge?page_id=bruhtus.visitor-badge"
alt="Visitor" />
<a/>

<a href="http://vim.liuchengxu.org/">
<img src="https://img.shields.io/badge/%F0%9F%94%A7editor-space~vim-blue" alt="Editor: space-vim">
<a/>
